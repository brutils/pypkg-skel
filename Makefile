SHELL           := /usr/bin/zsh

# The following variables should be defined in
# commandline
# -----------------------------------------------------
# PROJECT_NAME	:=
# PROJECT_DESC	:=
# GIT		:=

TARGET		=  dist/$(subst _,-,$(NAME))
DEFAULT_NAME	:= project_name_in_snake_case
DEFAULT_DESC	:= New Project
DEFAULT_AUTHOR	:= Raghav B. Venkataramaiyer
DEFAULT_A_EMAIL	:= bvraghav@gmail.com

read		= $(shell 				\
	A="$2"; vared -hep "$1" A; echo $$A		\
)

ifndef NAME
NAME		:= $(or $(NAME),$(DEFAULT_NAME))
NAME		:= $(call read,Name: ,$(NAME))
NAME		:= $(subst -,_,$(NAME))
endif

ifndef DESC
DESC		:= $(or $(DESC),$(DEFAULT_DESC))
DESC		:= $(call read,Desc: ,$(DESC))
endif

ifndef GIT
GIT		:= $(call read,Git: ,$(GIT))
endif

ifndef AUTHOR
AUTHOR		:= $(call 				\
  read,Author: ,$(or $(AUTHOR),$(DEFAULT_AUTHOR)))
endif

ifndef AUTHOR_EMAIL
AUTHOR_EMAIL	:= $(or $(AUTHOR_EMAIL),$(DEFAULT_A_EMAIL))
AUTHOR_EMAIL	:= $(call 				\
  read,Author Email: ,$(AUTHOR_EMAIL))
endif

ifndef DATE
DATE		:= $(shell date +"%b %Y")
DATE		:= $(call read,Date: ,$(DATE))
endif

ifdef UNDEFS
ERRORS		+= Required variables undefined: $(UNDEFS).
endif

ifdef ERRORS
$(error ${ERRORS})
endif

ifdef WARNINGS
$(warning ${WARNINGS})
endif

m4		= m4					\
	   -DPROJECT_NAME="$(NAME)"			\
	   -DPROJECT_DESC="$(DESC)"			\
	   -DGIT="$(GIT)"				\
	   -DAUTHOR="$(AUTHOR)"				\
	   -DAUTHOR_EMAIL="$(AUTHOR_EMAIL)"		\
	   -DDATE="$(DATE)"				\


all :
	$(info NAME:$(NAME))
	$(info DESC:$(DESC))
	$(info GIT:$(GIT))
	$(info AUTHOR:$(AUTHOR))
	$(info AUTHOR_EMAIL:$(AUTHOR_EMAIL))
	$(info DATE:$(DATE))
	$(info TARGET:$(TARGET))
	-mkdir -p $(TARGET)/tests
	find skel -maxdepth 1 -mindepth 1		\
	     -not -iname '*.in'				\
	     -and -not -iname '*~'			\
	     -and -not -iname 'tests'			\
	     -exec cp -n -r -t $(TARGET) {} +
#	rm $(TARGET)/*.in
	[ ! -f $(TARGET)/pyproject.toml ] && 		\
	  $(m4) skel/pyproject.toml.in			\
	        > $(TARGET)/pyproject.toml
	[ ! -f $(TARGET)/LICENSE ] && 			\
	  $(m4) skel/LICENSE.mit.in			\
	        > $(TARGET)/LICENSE
	[ ! -f $(TARGET)/README.md ] && 		\
	  $(m4) skel/README.md.in			\
	        > $(TARGET)/README.md
	[ ! -f $(TARGET)/mkdocs.yml ] && 		\
	  $(m4) skel/mkdocs.yml.in			\
	        > $(TARGET)/mkdocs.yml
	[ ! -f $(TARGET)/tests/test_add_one.py ] &&	\
	  $(m4) skel/tests/test_add_one.py.in		\
	        > $(TARGET)/tests/test_add_one.py
	[ -d $(TARGET)/src/$(NAME) ] && {	\
	  rm -rf $(TARGET)/src/PROJECT_NAME		\
	} || {						\
	  mv $(TARGET)/src/PROJECT_NAME			\
	     $(TARGET)/src/$(NAME)			\
	}
clean :
	rm -rf dist
