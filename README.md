# PyPkg

Inspired by Arch’s `makepkg -si`; so that there’s
something like `make [args]` and the skeleton gets populated.

## Usage

In order to create a python project `mypkg` under
current working directory, with `pypkg-skel` working
copy at `~/code/brutils/pypkg-skel`, issue this command
on shell and follow the prompts.

```sh
make -C ~/code/brutils/pypkg-skel TARGET=${PWD}/mypkg
```
